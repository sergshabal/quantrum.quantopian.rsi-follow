"""
Quantrum: Следование за RSI(10/14/20)
"""

import talib


# Setup our variables
def initialize(context):
    # Indices: 'DIA', 'QQQ', 'SPY', 'IWM'
    # Sectors: 'XLY', 'XLK', 'XLI', 'XLB', 'XLE', 'XLP', 'XLV', 'XLU', 'XLF', 'XLRE'
    #context.stocks = symbols('SPY')
    context.stocks = symbols('XLY', 'XLK', 'XLI', 'XLB', 'XLE', 'XLP', 'XLV', 'XLU', 'XLF', 'XLRE')

    context.RSI_PERIOD = 20  # RSI period
    context.RSI_LOW, context.RSI_HIGH = 45, 55  # signals thresholds
    context.RSI_HOLD_DEVIATION = 10  # hold range
    # Sectors:
    # только для длинных 10(+304%), 11(+340%), 12(+322%), 13(+278%), 14(+265%)
    # в обоих направлениях 10(+227%), 11(+259%), 12(+241%), 13(+205%), 14(+196%)

    schedule_function(rsi_follow, date_rules.every_day(), time_rules.market_open())

def rsi_follow(context, data):
    """
    Стратегия следования на дневных графиках.
    Входим на 55/45. Стоп на 45/55.
    """
    can_trade = data.can_trade(context.stocks)

    # Load historical data for the stocks
    prices = data.history(context.stocks, 'price', 500, '1d')

    trades = {
        'long': [],
        'short': []
    }
    hold = {
        'long': [],
        'short': []
    }

    # Loop through our list of stocks
    for stock in context.stocks:
        # skip stocks
        if not can_trade[stock]:
            continue

        # calculate RSI
        arr_rsi = talib.RSI(prices[stock], timeperiod=context.RSI_PERIOD)
        # use RSI of daily close
        rsi = arr_rsi[-2]
        rsi_prev = arr_rsi[-3]

        # signal to open on enter
        long_allow = rsi > context.RSI_HIGH and rsi_prev < context.RSI_HIGH
        short_allow = rsi < context.RSI_LOW and rsi_prev > context.RSI_LOW

        # get smoothed rsi
        """
        sma_rsi = talib.SMA(arr_rsi, timeperiod=context.RSI_PERIOD * 2)
        rsiXsma_long = rsi > sma_rsi[-2]
        rsiXsma_short = rsi < sma_rsi[-2]
        """

        # signal to hold
        long_hold = rsi > context.RSI_HIGH - context.RSI_HOLD_DEVIATION
        short_hold = rsi < context.RSI_LOW + context.RSI_HOLD_DEVIATION

        # filter signals by sma200
        #"""
        sma_short = talib.SMA(prices[stock], timeperiod=20)
        sma_long = talib.SMA(prices[stock], timeperiod=200)
        # filter open
        long_allow = long_allow and sma_short[-1] > sma_long[-1]
        short_allow = short_allow and sma_short[-1] < sma_long[-1]
        # filter hold
        long_hold = long_hold and sma_short[-1] > sma_long[-1]
        short_hold = short_hold and sma_short[-1] < sma_long[-1]
        #"""

        if long_allow:
            trades['long'].append(stock)
        elif short_allow:
            trades['short'].append(stock)
            pass
        else:
            #log.warn('No signal for {0}'.format(stock.symbol))
            pass

        # hold only opened positions
        if context.portfolio.positions[stock].amount != 0:
            if long_hold:
                hold['long'].append(stock)
            elif short_hold:
                hold['short'].append(stock)

        # remove from hold if cross smoothed rsi
        """
        if stock in hold['long'] and rsiXsma_short:
            hold['long'].remove(stock)
        elif stock in hold['short'] and rsiXsma_long:
            hold['short'].remove(stock)
        """

        if len(context.stocks) < 3:
            record(**{'RSI_' + stock.symbol: rsi})
            #record(**{'PRICE_' + stock.symbol: current})
            #record(**{'SMA200_' + stock.symbol: sma[-1]})
            #record(**{'SMA_RSI_' + stock.symbol: sma_rsi[-2]})

    # open positions for the whole capital
    total_long = len(set(trades['long'] + hold['long']))
    total_short = len(set(trades['short'] + hold['short']))
    coef_long = 1./(total_long if total_long else 1)  # long is positive
    coef_short = 1./(total_short if total_short else 1)  # short is negative

    # disable direction
    #trades['short'] = []
    #hold['short'] = []

    for stock in context.stocks:
        # buy to long stock
        if stock in trades['long']:
            order_target_percent(stock, coef_long)
            print('long', stock.symbol, "{0:.2f} => {1:.2f}".format(rsi_prev, rsi))

        # sell to short stock
        elif stock in trades['short']:
            # negative percent
            order_target_percent(stock, -1 * coef_short)
            print('short', stock.symbol, "{0:.0f} => {1:.2f}".format(rsi_prev, rsi))

        # close position
        elif (context.portfolio.positions[stock].amount
              and stock not in hold['long'] and stock not in hold['short']):
            order_target_percent(stock, 0)
            print('close', stock.symbol, "{0:.2f} => {1:.2f}".format(rsi_prev, rsi))
            pass

        if len(context.stocks) < 3:
            record(**{stock.symbol: context.portfolio.positions[stock].amount})


# убрать подчеркивание и закомментировать schedule_function
def _handle_data(context, data):
    """
    Стратегия следования на часовых и выход на дневных графиках.
    """

    # check time
    dt = get_datetime()
    # run every hour - dt.minute in [59]
    # run every 30 minutes - dt.minute in [59, 29]
    # run every 15 minutes - dt.minute in [59, 14, 29, 44]
    if dt.minute not in [59]:
        return

    can_trade = data.can_trade(context.stocks)

    # Load historical data for the stocks
    prices = data.history(context.stocks, 'price', 500, '1d')

    # load minutes data
    prices_min = data.history(context.stocks, ['close'], 6000, '1m')
    tf = 'H'  # hour
    prices_hour = dict()
    for stock in context.stocks:
        prices_hour[stock] = prices_min['close'][stock].resample(tf).last().dropna()

    trades = {
        'long': [],
        'short': []
    }
    hold = {
        'long': [],
        'short': []
    }

    # Loop through our list of stocks
    for stock in context.stocks:
        # skip stocks
        if not can_trade[stock]:
            continue

        # calculate hourly RSI
        arr_rsi = talib.RSI(prices_hour[stock], timeperiod=context.RSI_PERIOD)
        # use RSI of hourly close
        rsi = arr_rsi[-1]
        rsi_prev = arr_rsi[-2]

        # signal to open on enter
        long_allow = rsi > context.RSI_HIGH and rsi_prev < context.RSI_HIGH
        short_allow = rsi < context.RSI_LOW and rsi_prev > context.RSI_LOW

        # signal to hold
        long_hold = rsi > context.RSI_LOW
        short_hold = rsi < context.RSI_HIGH

        # calculate daily RSI
        arr_daily_rsi = talib.RSI(prices[stock], timeperiod=context.RSI_PERIOD)
        daily_rsi = arr_daily_rsi[-2]  # yesterday close price (-2) OR last (-1)
        # add daily pos
        long_allow = long_allow and daily_rsi < context.RSI_HIGH
        short_allow = short_allow and daily_rsi > context.RSI_LOW
        # add daily pos
        long_hold = long_hold and daily_rsi < context.RSI_HIGH
        short_hold = short_hold and daily_rsi > context.RSI_LOW

        if long_allow:
            trades['long'].append(stock)
        elif short_allow:
            trades['short'].append(stock)
        else:
            #log.warn('No signal for {0}'.format(stock.symbol))
            pass

        # hold only opened positions
        if context.portfolio.positions[stock].amount != 0:
            if long_hold:
                hold['long'].append(stock)
            elif short_hold:
                hold['short'].append(stock)

        if len(context.stocks) < 3:
            record(**{'RSI_' + stock.symbol: rsi})
            #record(**{'PRICE_' + stock.symbol: current})
            #record(**{'SMA200_' + stock.symbol: sma[-1]})
            #record(**{'SMA_RSI_' + stock.symbol: sma_rsi[-2]})

    # open positions for the whole capital
    total_long = len(set(trades['long'] + hold['long']))
    total_short = len(set(trades['short'] + hold['short']))
    coef_long = 1./(total_long if total_long else 1)  # long is positive
    coef_short = 1./(total_short if total_short else 1)  # short is negative

    # disable direction
    trades['short'] = []
    hold['short'] = []

    for stock in context.stocks:
        # buy to long stock
        if stock in trades['long']:
            order_target_percent(stock, coef_long)
            print('long', stock.symbol, "{0:.2f} => {1:.2f} ({2:.2f})".format(rsi_prev, rsi, daily_rsi))

        # sell to short stock
        elif stock in trades['short']:
            # negative percent
            order_target_percent(stock, -1 * coef_short)
            print('short', stock.symbol, "{0:.2f} => {1:.2f} ({2:.2f})".format(rsi_prev, rsi, daily_rsi))

        # close position
        elif (context.portfolio.positions[stock].amount
              and stock not in hold['long'] and stock not in hold['short']):
            order_target_percent(stock, 0)
            print('close', stock.symbol, "{0:.2f} => {1:.2f} ({2:.2f})".format(rsi_prev, rsi, daily_rsi))
            pass

        if len(context.stocks) < 3:
            record(**{stock.symbol: context.portfolio.positions[stock].amount})

